package com.game.service;

import com.game.dto.RoomDto;

public class MoveService {

    public void moveRoom(RoomDto room) {
        if (room.isTreasure()) {
            System.out.println("Vous récuperez le trésor et vous quittez le donjon");
        } else {
            System.out.println("Vous quittez la salle par " + room.getDoor());
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            System.out.println("Error: sleep moveService");
        }
    }

}
