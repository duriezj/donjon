package com.game.service;


import com.game.dto.ActionDto;
import com.game.dto.RoomDto;

import java.util.List;

public class WatchService {

    public void watchRoom(String name, RoomDto room, List<ActionDto> actions) {
        System.out.println(name + ", vous etes attentif à chaque détail de la salle : ");
        if (room.isTreasure()) {
            System.out.println("vous observez le trésor du donjon");
        } else {
            System.out.println("vous observez " + room.getDoor());
        }
        if (room.getMonsters() != null && !room.getMonsters().isEmpty()) {
            String displayMonsters = room.getMonsters().size() > 1 ? " monstres" : " monstre";
            System.out.println("vous observez " + room.getMonsters().size() + displayMonsters);
        }
        if (room.getObjects() != null && !room.getObjects().isEmpty()) {
            String displayObjects = room.getObjects().size() > 1 ? " objets" : " objet";
            System.out.println("vous observez " + room.getObjects().size() + displayObjects);
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            System.out.println("Error : sleep Watch service");
        }
    }
}
