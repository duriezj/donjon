package com.game.service;

import com.game.dto.ActionDto;
import com.game.dto.RoomDto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ActionService {

    private WatchService watchService;
    private MoveService moveService;

    public ActionService() {
        watchService = new WatchService();
        moveService = new MoveService();
    }

    public List<ActionDto> generateActions() {
        List<ActionDto> actions = new ArrayList<>();

        ActionDto watchAction = new ActionDto("Regarder", "R", true, 1);
        ActionDto moveAction = new ActionDto("Se Déplacer", "D", false, 2);
        ActionDto fightAction = new ActionDto("Combattre", "C", false, 3);
        ActionDto useAction = new ActionDto("Utiliser un Objet", "O", false, 4);
        actions.add(watchAction);
        actions.add(moveAction);
        actions.add(fightAction);
        actions.add(useAction);

        return actions;
    }

    public List<ActionDto> manageActions(String name, List<ActionDto> actions, RoomDto room) {
        System.out.println("Que voulez-vous faire " + name + " ?");
        actions.stream()
                .filter(action -> action.isEnable())
                .sorted(Comparator.comparing(ActionDto::getOrder))
                .forEach(action -> {
                    System.out.println("(" + action.getType() + ") " + action.getName());
                });

        boolean valid = false;
        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!valid) {
            final String inputStream = scanner.next();
            valid = actions.stream()
                    .filter(action -> action.isEnable())
                    .anyMatch(action -> action.getType().equalsIgnoreCase(inputStream)
                            || action.getName().equalsIgnoreCase(inputStream));
            if (!valid) {
                System.out.println("valeur incorrecte");
            } else {
                input = inputStream;
            }
        }
        return manageAction(name, input.toLowerCase(), room, actions);

    }

    private List<ActionDto> manageAction(String name, String input, RoomDto room, List<ActionDto> actions) {
        switch (input) {
            case "r":
                watchService.watchRoom(name, room, actions);
                break;
            case "d":
                moveService.moveRoom(room);
                break;
        }
        return updateActions(room, actions);
    }

    private List<ActionDto> updateActions(RoomDto room, List<ActionDto> actions) {
        return actions.stream().map(action -> {
            switch (action.getType()) {
                case "D":
                    if (room.getMonsters() != null && !room.getMonsters().isEmpty()) {
                        action.setEnable(false);
                    } else {
                        action.setEnable(true);
                    }
                    break;
                default:
                    action.setEnable(true);
            }
            return action;
        }).collect(Collectors.toList());
    }
}
