package com.game.service;

import com.game.dto.Characters.MonsterDto;
import com.game.dto.Objects.*;
import com.game.dto.RoomDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RoomService {
    public static final int LEVEL_MAX = 5;

    public RoomDto generateRoom(int level) {
        RoomDto room = new RoomDto();
        if (level == LEVEL_MAX) {
            room.setTreasure(true);
        } else {
            room.setDoor(generateDoor());
        }
        Random r = new Random();
        int monstersNumber = r.nextInt(level * 2);
        room.setMonsters(generateMonster(monstersNumber, level));
        int objectsNumber = r.nextInt(level * 2);
        room.setObjects(generateObjects(objectsNumber));
        return room;
    }

    private List<MonsterDto> generateMonster(int nb, int level) {
        List<MonsterDto> monsters = new ArrayList<>();
        for (int i = 0; i < nb; i++) {
            MonsterDto monster = new MonsterDto(3 + level, 3, 5 + level);
            monsters.add(monster);
        }
        return monsters;
    }

    private List<ObjectDto> generateObjects(int nb) {
        List<ObjectDto> objects = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i < nb; i++) {
            int typeObject = r.nextInt(4);
            switch (typeObject) {
                case 1:
                    PotionsLifeDto potionsLifeDto = new PotionsLifeDto(3);
                    objects.add(potionsLifeDto);
                    break;
                case 2:
                    PotionStrengthDto potionStrengthDto = new PotionStrengthDto(2);
                    objects.add(potionStrengthDto);
                    break;
                case 3:
                    ArmedBanditDto armedBanditDto = new ArmedBanditDto();
                    objects.add(armedBanditDto);
                    break;
                default:
                    GoldPurseDto goldPurseDto = new GoldPurseDto(5);
                    objects.add(goldPurseDto);
                    break;
            }
        }
        return objects;
    }

    private String generateDoor() {
        Random r = new Random();
        int cardinal = r.nextInt(4);
        switch (cardinal) {
            case 1:
                return "une porte au nord";
            case 2:
                return "une porte au sud";
            case 3:
                return "une porte à l'est";
            default:
                return "une porte à l'ouest";
        }
    }
}
