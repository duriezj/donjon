package com.game.dto.Characters;

public class MonsterDto extends CharacterDto {

    private int goldToPickUp;


    public MonsterDto() {
        super();
    }

    public MonsterDto(int lifePoint, int strengthPoint, int goldToPickUp) {
        super(lifePoint, strengthPoint);
        this.goldToPickUp = goldToPickUp;
    }

    public int getGoldToPickUp() {
        return goldToPickUp;
    }

    public void setGoldToPickUp(int goldToPickUp) {
        this.goldToPickUp = goldToPickUp;
    }
}
