package com.game.dto.Characters;

public class PlayerDto extends CharacterDto {

    private String name;
    private String gold;

    public PlayerDto() {
    }

    public PlayerDto(int lifePoint, int strengthPoint, String name) {
        super(lifePoint, strengthPoint);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGold() {
        return gold;
    }

    public void setGold(String gold) {
        this.gold = gold;
    }

    public void addGold(String gold) {
        this.gold = gold;
    }
}
