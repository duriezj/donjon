package com.game.dto.Characters;

public abstract class CharacterDto {

    private int lifePoint;
    private int strengthPoint;


    public CharacterDto() {
    }

    public CharacterDto(int lifePoint, int strengthPoint) {
        this.lifePoint = lifePoint;
        this.strengthPoint = strengthPoint;
    }

    public int getLifePoint() {
        return lifePoint;
    }

    public void setLifePoint(int lifePoint) {
        this.lifePoint = lifePoint;
    }

    public void addLife(int lifePoint) {
        this.lifePoint += lifePoint;
    }

    public int getStrengthPoint() {
        return strengthPoint;
    }

    public void setStrengthPoint(int strengthPoint) {
        this.strengthPoint = strengthPoint;
    }

    public void addStrengh(int strenghPoint) {
        this.strengthPoint += strenghPoint;
    }
}
