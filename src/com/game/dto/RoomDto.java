package com.game.dto;

import com.game.dto.Characters.MonsterDto;
import com.game.dto.Objects.ObjectDto;

import java.util.List;

public class RoomDto {

    private List<ObjectDto> objects;
    private List<MonsterDto> monsters;
    private String door;
    private boolean Treasure;

    public List<ObjectDto> getObjects() {
        return objects;
    }

    public void setObjects(List<ObjectDto> objects) {
        this.objects = objects;
    }

    public List<MonsterDto> getMonsters() {
        return monsters;
    }

    public void setMonsters(List<MonsterDto> monsters) {
        this.monsters = monsters;
    }

    public String getDoor() {
        return door;
    }

    public void setDoor(String door) {
        this.door = door;
    }

    public boolean isTreasure() {
        return Treasure;
    }

    public void setTreasure(boolean treasure) {
        Treasure = treasure;
    }
}
