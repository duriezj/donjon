package com.game.dto.Objects;

public abstract class PotionDto extends ObjectDto {
    private int points;

    public PotionDto(String name, int points) {
        super(name);
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
