package com.game.dto.Objects;

public abstract class ObjectDto {
    private String name;

    public ObjectDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
