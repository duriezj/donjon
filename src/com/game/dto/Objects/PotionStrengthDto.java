package com.game.dto.Objects;

public class PotionStrengthDto extends PotionDto {

    public PotionStrengthDto(int points) {
        super("Potion de force", points);
    }
}
