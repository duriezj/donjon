package com.game.dto.Objects;

public class PotionsLifeDto extends PotionDto {

    public PotionsLifeDto(int points) {
        super("Potion de vie", points);
    }
}
