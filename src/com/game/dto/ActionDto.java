package com.game.dto;

public class ActionDto {
    private String name;
    private boolean enable;
    private String type;
    private int order;

    public ActionDto() {

    }

    public ActionDto(String name, String type, boolean enable, int order) {
        this.name = name;
        this.type = type;
        this.enable = enable;
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
