package com.game;

import com.game.dto.ActionDto;
import com.game.dto.RoomDto;
import com.game.service.ActionService;
import com.game.service.RoomService;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        RoomService roomService = new RoomService();
        RoomDto room = roomService.generateRoom(5);
        ActionService actionService = new ActionService();
        List<ActionDto> actions = actionService.generateActions();
        actionService.manageActions("Robert", actions, room);
    }
}
